fn main() -> Result<(), std::io::Error> {
    const COPY_ATTR: &str = "#[derive(Copy)]";
    tonic_build::configure()
        .out_dir("src/generated")
        .build_client(false)
        //.type_attribute(".", "#[derive(serde::Serialize,serde::Deserialize)]")
        // .type_attribute(".", COPY_ATTR)
        .type_attribute("Single", COPY_ATTR)
        .type_attribute("template.Single.x", COPY_ATTR)
        .type_attribute("AutoTranslationEntry", COPY_ATTR)
        .type_attribute("Double", COPY_ATTR)
        .type_attribute("Echo", COPY_ATTR)
        .type_attribute("Echo.type", COPY_ATTR)
        .type_attribute("Territory", COPY_ATTR)
        .type_attribute("World", COPY_ATTR)
        .type_attribute("Location", COPY_ATTR)
        .type_attribute("Realm", COPY_ATTR)
        .include_file("mod.rs")
        //.proto_path("../../proto")
        .compile(
            &[
                "../../proto/api/echoshell.proto",
                // "../../proto/echo/sound.proto",
                // "../../proto/echo/concept/adversary.proto",
                // "../../proto/echo/concept/thing.proto",
                // "../../proto/echo/concept/conjunction.proto",
                // "../../proto/echo/sentence/template.proto",
            ],
            &["../../proto"],
        )
    //.compile_with_config(, protos, includes);
    //tonic_build::compile_protos("../../proto/shell.proto")?;
    //Ok(())
}
