#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum Conjunction {
    AndThen = 0,
    Or = 1,
    But = 2,
    Therefore = 3,
    InShort = 4,
    Except = 5,
    ByTheWay = 6,
    SoToSpeak = 7,
    AllTheMore = 8,
    Comma = 9,
    Fullstop = 10,
}
impl Conjunction {
    /// String value of the enum field names used in the ProtoBuf definition.
    ///
    /// The values are not transformed in any way and thus are considered stable
    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
    pub fn as_str_name(&self) -> &'static str {
        match self {
            Conjunction::AndThen => "AndThen",
            Conjunction::Or => "Or",
            Conjunction::But => "But",
            Conjunction::Therefore => "Therefore",
            Conjunction::InShort => "InShort",
            Conjunction::Except => "Except",
            Conjunction::ByTheWay => "ByTheWay",
            Conjunction::SoToSpeak => "SoToSpeak",
            Conjunction::AllTheMore => "AllTheMore",
            Conjunction::Comma => "Comma",
            Conjunction::Fullstop => "Fullstop",
        }
    }
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Single {
    #[prost(enumeration="PhraseTemplate", tag="1")]
    pub template: i32,
    #[prost(oneof="single::X", tags="2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13")]
    pub x: ::core::option::Option<single::X>,
}
/// Nested message and enum types in `Single`.
pub mod single {
    #[derive(Copy)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum X {
        #[prost(enumeration="super::x::Action", tag="2")]
        Action(i32),
        #[prost(enumeration="super::x::Adversary", tag="3")]
        Adversary(i32),
        #[prost(enumeration="super::x::Affinity", tag="4")]
        Affinity(i32),
        #[prost(enumeration="super::x::BattleTactic", tag="5")]
        BattleTactic(i32),
        #[prost(enumeration="super::x::BodyPart", tag="6")]
        BodyPart(i32),
        #[prost(enumeration="super::x::Concept", tag="7")]
        Concept(i32),
        #[prost(enumeration="super::x::Direction", tag="8")]
        Direction(i32),
        #[prost(enumeration="super::x::People", tag="9")]
        People(i32),
        #[prost(enumeration="super::x::Phrase", tag="10")]
        Phrase(i32),
        #[prost(enumeration="super::x::Place", tag="11")]
        Place(i32),
        #[prost(enumeration="super::x::Situation", tag="12")]
        Situation(i32),
        #[prost(enumeration="super::x::Thing", tag="13")]
        Thing(i32),
    }
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Double {
    #[prost(message, optional, tag="1")]
    pub first: ::core::option::Option<Single>,
    #[prost(message, optional, tag="2")]
    pub second: ::core::option::Option<Single>,
    #[prost(enumeration="Conjunction", tag="3")]
    pub conjunction: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SingleEmote {
    #[prost(message, optional, tag="1")]
    pub sentence: ::core::option::Option<Single>,
    #[prost(uint32, tag="2")]
    pub emote_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DoubleEmote {
    #[prost(message, optional, tag="1")]
    pub sentence: ::core::option::Option<Double>,
    #[prost(uint32, tag="2")]
    pub emote_id: u32,
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AutoTranslationEntry {
    #[prost(uint32, tag="1")]
    pub group: u32,
    #[prost(uint32, tag="2")]
    pub row: u32,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum PhraseTemplate {
    XAhead = 0,
    LikelyX = 1,
    IfOnlyIHadAxDotDotDot = 2,
    XComma = 3,
    Ox = 4,
    AhhCommaXDotDotDot = 5,
    NoXAhead = 6,
    FirstOffCommaX = 7,
    DidNotExpectXDotDotDot = 8,
    BeholdCommaXExclamation = 9,
    X = 10,
    XRequiredAhead = 11,
    SeekX = 12,
    VisionsOfXDotDotDot = 13,
    OfferX = 14,
    XExclamation = 15,
    BewaryofX = 16,
    StillNoXDotDotDot = 17,
    CouldThisBeAxQuestion = 18,
    PraiseTheX = 19,
    XQuestion = 20,
    TryX = 21,
    WhyIsItAlwaysXQuestion = 22,
    TimeForX = 23,
    LetThereBeX = 24,
    XDotDotDot = 25,
}
impl PhraseTemplate {
    /// String value of the enum field names used in the ProtoBuf definition.
    ///
    /// The values are not transformed in any way and thus are considered stable
    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
    pub fn as_str_name(&self) -> &'static str {
        match self {
            PhraseTemplate::XAhead => "XAhead",
            PhraseTemplate::LikelyX => "LikelyX",
            PhraseTemplate::IfOnlyIHadAxDotDotDot => "IfOnlyIHadAXDotDotDot",
            PhraseTemplate::XComma => "XComma",
            PhraseTemplate::Ox => "OX",
            PhraseTemplate::AhhCommaXDotDotDot => "AhhCommaXDotDotDot",
            PhraseTemplate::NoXAhead => "NoXAhead",
            PhraseTemplate::FirstOffCommaX => "FirstOffCommaX",
            PhraseTemplate::DidNotExpectXDotDotDot => "DidNotExpectXDotDotDot",
            PhraseTemplate::BeholdCommaXExclamation => "BeholdCommaXExclamation",
            PhraseTemplate::X => "X",
            PhraseTemplate::XRequiredAhead => "XRequiredAhead",
            PhraseTemplate::SeekX => "SeekX",
            PhraseTemplate::VisionsOfXDotDotDot => "VisionsOfXDotDotDot",
            PhraseTemplate::OfferX => "OfferX",
            PhraseTemplate::XExclamation => "XExclamation",
            PhraseTemplate::BewaryofX => "BewaryofX",
            PhraseTemplate::StillNoXDotDotDot => "StillNoXDotDotDot",
            PhraseTemplate::CouldThisBeAxQuestion => "CouldThisBeAXQuestion",
            PhraseTemplate::PraiseTheX => "PraiseTheX",
            PhraseTemplate::XQuestion => "XQuestion",
            PhraseTemplate::TryX => "TryX",
            PhraseTemplate::WhyIsItAlwaysXQuestion => "WhyIsItAlwaysXQuestion",
            PhraseTemplate::TimeForX => "TimeForX",
            PhraseTemplate::LetThereBeX => "LetThereBeX",
            PhraseTemplate::XDotDotDot => "XDotDotDot",
        }
    }
}
