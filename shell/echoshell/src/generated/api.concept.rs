// option csharp_namespace = "Echo.Concept";

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum Adversary {
    Enemy = 0,
    WeakFoe = 1,
    StrongFoe = 2,
    Monster = 3,
    Dragon = 4,
    Boss = 5,
    Sentry = 6,
    Group = 7,
}
// option csharp_namespace = "Echo.Concept";

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum Thing {
    Item = 0,
    NecessaryItem = 1,
    PreciousItem = 2,
    Something = 3,
    SomethingIncredible = 4,
    TreasureChest = 5,
    Corpse = 6,
    Coffin = 7,
    Trap = 8,
}
// option csharp_namespace = "Echo.Concept";

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum Conjunction {
    AndThen = 0,
    Or = 1,
    But = 2,
    Therefore = 3,
    InShort = 4,
    Except = 5,
    ByTheWay = 6,
    SoToSpeak = 7,
    AllTheMore = 8,
    Comma = 9,
    Fullstop = 10,
}
