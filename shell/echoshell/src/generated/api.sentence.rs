#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum Conjunction {
    AndThen = 0,
    Or = 1,
    But = 2,
    Therefore = 3,
    InShort = 4,
    Except = 5,
    ByTheWay = 6,
    SoToSpeak = 7,
    AllTheMore = 8,
    Comma = 9,
    Fullstop = 10,
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Single {
    #[prost(enumeration="Phrase", tag="1")]
    pub template: i32,
    #[prost(oneof="single::X", tags="2, 3")]
    pub x: ::core::option::Option<single::X>,
}
/// Nested message and enum types in `Single`.
pub mod single {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum X {
        #[prost(enumeration="super::x::Adversary", tag="2")]
        Adversary(i32),
        #[prost(enumeration="super::x::Thing", tag="3")]
        Thing(i32),
    }
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Double {
    #[prost(message, optional, tag="1")]
    pub first: ::core::option::Option<Single>,
    #[prost(message, optional, tag="2")]
    pub second: ::core::option::Option<Single>,
    #[prost(enumeration="Conjunction", tag="3")]
    pub conjunction: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SingleEmote {
    #[prost(message, optional, tag="1")]
    pub sentence: ::core::option::Option<Single>,
    #[prost(uint32, tag="2")]
    pub emote_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DoubleEmote {
    #[prost(message, optional, tag="1")]
    pub sentence: ::core::option::Option<Double>,
    #[prost(uint32, tag="2")]
    pub emote_id: u32,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum Phrase {
    XAhead = 0,
    LikelyX = 1,
    IfOnlyIHadAxDotDotDot = 2,
    XComma = 3,
    Ox = 4,
    AhhCommaXDotDotDot = 5,
    NoXAhead = 6,
    FirstOffCommaX = 7,
    DidNotExpectXDotDotDot = 8,
    BeholdCommaXExclamation = 9,
    X = 10,
    XRequiredAhead = 11,
    SeekX = 12,
    VisionsOfXDotDotDot = 13,
    OfferX = 14,
    XExclamation = 15,
    BewaryofX = 16,
    StillNoXDotDotDot = 17,
    CouldThisBeAxQuestion = 18,
    PraiseTheX = 19,
    XQuestion = 20,
    TryX = 21,
    WhyIsItAlwaysXQuestion = 22,
    TimeForX = 23,
    LetThereBeX = 24,
    XDotDotDot = 25,
}
