#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Echoes {
    #[prost(message, optional, tag="1")]
    pub territory: ::core::option::Option<Territory>,
    #[prost(message, repeated, tag="2")]
    pub echo: ::prost::alloc::vec::Vec<Echo>,
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Echo {
    ///  Micro Creation Epoch
    #[prost(uint64, tag="1")]
    pub id: u64,
    #[prost(float, tag="4")]
    pub x: f32,
    #[prost(float, tag="5")]
    pub y: f32,
    #[prost(float, tag="6")]
    pub z: f32,
    #[prost(bool, tag="7")]
    pub spoiler: bool,
    #[prost(uint32, tag="8")]
    pub up_votes: u32,
    #[prost(uint32, tag="9")]
    pub down_votes: u32,
    ///  TODO: Instead of single/double templates, AutoTranslationEntries
    ///   however, how many? A list without limites? Still templates for
    ///   conjunctions?
    #[prost(message, optional, tag="10")]
    pub entry: ::core::option::Option<template::AutoTranslationEntry>,
    #[prost(oneof="echo::Type", tags="2, 3")]
    pub r#type: ::core::option::Option<echo::Type>,
}
/// Nested message and enum types in `Echo`.
pub mod echo {
    #[derive(Copy)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Type {
        #[prost(message, tag="2")]
        Single(super::template::Single),
        #[prost(message, tag="3")]
        Double(super::template::Double),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Whisper {
    #[prost(message, optional, tag="1")]
    pub echo: ::core::option::Option<Echo>,
    #[prost(message, optional, tag="2")]
    pub territory: ::core::option::Option<Territory>,
    #[prost(uint32, tag="3")]
    pub world: u32,
    #[prost(uint32, tag="4")]
    pub data_center: u32,
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Territory {
    #[prost(uint32, tag="1")]
    pub v: u32,
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Location {
    #[prost(message, optional, tag="1")]
    pub territory: ::core::option::Option<Territory>,
    #[prost(float, tag="2")]
    pub x: f32,
    #[prost(float, tag="3")]
    pub y: f32,
    #[prost(float, tag="4")]
    pub z: f32,
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct World {
    #[prost(message, optional, tag="1")]
    pub location: ::core::option::Option<Location>,
    #[prost(uint32, tag="2")]
    pub world: u32,
}
#[derive(Copy)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Realm {
    #[prost(message, optional, tag="1")]
    pub location: ::core::option::Option<Location>,
    #[prost(uint32, tag="2")]
    pub realm: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Pearlless {
    #[prost(string, tag="1")]
    pub character_name: ::prost::alloc::string::String,
    #[prost(string, tag="2")]
    pub world: ::prost::alloc::string::String,
    #[prost(string, tag="3")]
    pub pub_key: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct PearlChallenge {
    #[prost(string, tag="1")]
    pub challenge: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Pearl {
    #[prost(bool, tag="1")]
    pub success: bool,
}
/// Generated server implementations.
pub mod echoshell_server {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    ///Generated trait containing gRPC methods that should be implemented for use with EchoshellServer.
    #[async_trait]
    pub trait Echoshell: Send + Sync + 'static {
        /// Whisper into the aether
        async fn utter(
            &self,
            request: tonic::Request<super::Whisper>,
        ) -> Result<tonic::Response<super::Echo>, tonic::Status>;
        /// Hear echoes across all shards around Location
        async fn hear(
            &self,
            request: tonic::Request<super::Location>,
        ) -> Result<tonic::Response<super::Echoes>, tonic::Status>;
        /// Hear echoes from own shard around Location (same World)
        async fn hear_own(
            &self,
            request: tonic::Request<super::World>,
        ) -> Result<tonic::Response<super::Echoes>, tonic::Status>;
        /// Hear echoes from nearby shards around Location (same Data Center)
        async fn hear_nearby(
            &self,
            request: tonic::Request<super::Realm>,
        ) -> Result<tonic::Response<super::Echoes>, tonic::Status>;
        /// Acquire a challenge from the Shell (Register)
        async fn acquire(
            &self,
            request: tonic::Request<super::Pearlless>,
        ) -> Result<tonic::Response<super::PearlChallenge>, tonic::Status>;
        /// Tell Shell to confirm challenge
        async fn confirm(
            &self,
            request: tonic::Request<super::PearlChallenge>,
        ) -> Result<tonic::Response<super::Pearl>, tonic::Status>;
    }
    #[derive(Debug)]
    pub struct EchoshellServer<T: Echoshell> {
        inner: _Inner<T>,
        accept_compression_encodings: EnabledCompressionEncodings,
        send_compression_encodings: EnabledCompressionEncodings,
    }
    struct _Inner<T>(Arc<T>);
    impl<T: Echoshell> EchoshellServer<T> {
        pub fn new(inner: T) -> Self {
            Self::from_arc(Arc::new(inner))
        }
        pub fn from_arc(inner: Arc<T>) -> Self {
            let inner = _Inner(inner);
            Self {
                inner,
                accept_compression_encodings: Default::default(),
                send_compression_encodings: Default::default(),
            }
        }
        pub fn with_interceptor<F>(
            inner: T,
            interceptor: F,
        ) -> InterceptedService<Self, F>
        where
            F: tonic::service::Interceptor,
        {
            InterceptedService::new(Self::new(inner), interceptor)
        }
        /// Enable decompressing requests with the given encoding.
        #[must_use]
        pub fn accept_compressed(mut self, encoding: CompressionEncoding) -> Self {
            self.accept_compression_encodings.enable(encoding);
            self
        }
        /// Compress responses with the given encoding, if the client supports it.
        #[must_use]
        pub fn send_compressed(mut self, encoding: CompressionEncoding) -> Self {
            self.send_compression_encodings.enable(encoding);
            self
        }
    }
    impl<T, B> tonic::codegen::Service<http::Request<B>> for EchoshellServer<T>
    where
        T: Echoshell,
        B: Body + Send + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = std::convert::Infallible;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(
            &mut self,
            _cx: &mut Context<'_>,
        ) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/api.Echoshell/Utter" => {
                    #[allow(non_camel_case_types)]
                    struct UtterSvc<T: Echoshell>(pub Arc<T>);
                    impl<T: Echoshell> tonic::server::UnaryService<super::Whisper>
                    for UtterSvc<T> {
                        type Response = super::Echo;
                        type Future = BoxFuture<
                            tonic::Response<Self::Response>,
                            tonic::Status,
                        >;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Whisper>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).utter(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = UtterSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec)
                            .apply_compression_config(
                                accept_compression_encodings,
                                send_compression_encodings,
                            );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/api.Echoshell/Hear" => {
                    #[allow(non_camel_case_types)]
                    struct HearSvc<T: Echoshell>(pub Arc<T>);
                    impl<T: Echoshell> tonic::server::UnaryService<super::Location>
                    for HearSvc<T> {
                        type Response = super::Echoes;
                        type Future = BoxFuture<
                            tonic::Response<Self::Response>,
                            tonic::Status,
                        >;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Location>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).hear(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = HearSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec)
                            .apply_compression_config(
                                accept_compression_encodings,
                                send_compression_encodings,
                            );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/api.Echoshell/HearOwn" => {
                    #[allow(non_camel_case_types)]
                    struct HearOwnSvc<T: Echoshell>(pub Arc<T>);
                    impl<T: Echoshell> tonic::server::UnaryService<super::World>
                    for HearOwnSvc<T> {
                        type Response = super::Echoes;
                        type Future = BoxFuture<
                            tonic::Response<Self::Response>,
                            tonic::Status,
                        >;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::World>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).hear_own(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = HearOwnSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec)
                            .apply_compression_config(
                                accept_compression_encodings,
                                send_compression_encodings,
                            );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/api.Echoshell/HearNearby" => {
                    #[allow(non_camel_case_types)]
                    struct HearNearbySvc<T: Echoshell>(pub Arc<T>);
                    impl<T: Echoshell> tonic::server::UnaryService<super::Realm>
                    for HearNearbySvc<T> {
                        type Response = super::Echoes;
                        type Future = BoxFuture<
                            tonic::Response<Self::Response>,
                            tonic::Status,
                        >;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Realm>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).hear_nearby(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = HearNearbySvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec)
                            .apply_compression_config(
                                accept_compression_encodings,
                                send_compression_encodings,
                            );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/api.Echoshell/Acquire" => {
                    #[allow(non_camel_case_types)]
                    struct AcquireSvc<T: Echoshell>(pub Arc<T>);
                    impl<T: Echoshell> tonic::server::UnaryService<super::Pearlless>
                    for AcquireSvc<T> {
                        type Response = super::PearlChallenge;
                        type Future = BoxFuture<
                            tonic::Response<Self::Response>,
                            tonic::Status,
                        >;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Pearlless>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).acquire(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = AcquireSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec)
                            .apply_compression_config(
                                accept_compression_encodings,
                                send_compression_encodings,
                            );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/api.Echoshell/Confirm" => {
                    #[allow(non_camel_case_types)]
                    struct ConfirmSvc<T: Echoshell>(pub Arc<T>);
                    impl<T: Echoshell> tonic::server::UnaryService<super::PearlChallenge>
                    for ConfirmSvc<T> {
                        type Response = super::Pearl;
                        type Future = BoxFuture<
                            tonic::Response<Self::Response>,
                            tonic::Status,
                        >;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::PearlChallenge>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).confirm(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = ConfirmSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec)
                            .apply_compression_config(
                                accept_compression_encodings,
                                send_compression_encodings,
                            );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => {
                    Box::pin(async move {
                        Ok(
                            http::Response::builder()
                                .status(200)
                                .header("grpc-status", "12")
                                .header("content-type", "application/grpc")
                                .body(empty_body())
                                .unwrap(),
                        )
                    })
                }
            }
        }
    }
    impl<T: Echoshell> Clone for EchoshellServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self {
                inner,
                accept_compression_encodings: self.accept_compression_encodings,
                send_compression_encodings: self.send_compression_encodings,
            }
        }
    }
    impl<T: Echoshell> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: Echoshell> tonic::server::NamedService for EchoshellServer<T> {
        const NAME: &'static str = "api.Echoshell";
    }
}
