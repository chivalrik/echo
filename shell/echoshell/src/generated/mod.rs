pub mod api {
    pub mod template {
        pub mod x {
            include!("api.template.x.rs");
        }
        include!("api.template.rs");
    }
    include!("api.rs");
}
