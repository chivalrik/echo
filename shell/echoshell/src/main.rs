use tonic::transport::Server;
use tower_http::trace::TraceLayer;
use tracing::{info, Level};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
// TODO: Clap ENV/CMD args
//  -> One arg for prefix before ContentId
//  -> One arg for hash algorithm (although prefix is should be enough)
//  -> Also some pepper for the passwords
//    -> Needed if using WebAuth /Public/Private Auth?
//  TODO: Oauth password/PKCE flow? Or 'simple' use TOTP?
// =>
// - Server needs to be able to verify character claim
// - Only info server has access to, which can be seen as reliable, is character name + world on lodestone (and with that, lodestone id in the url)
// - Client says: I am Character named X on World Y, here's my pub key
// - Server looks up that character on lodestone (via search?), checks Id and asks users to put token in profile
// - User puts that token into profile
// - Server verifies token and accepts previous pub key as belonging to the scrapped lodestone id
// - Client saves locally an association from localContentID to keypair and uses the pub key in further communication with the server.
// If
// - Server already has a pubkey for that lodestoneid, server starts registration process (else enumeration attack)
// - However, Client also shows user helpful message how to export/import their key from their other PC
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(
            std::env::var("RUST_LOG")
                .unwrap_or_else(|_| "echoshell=debug,tower_http=debug,tonic=debug".into()),
        ))
        .with(tracing_subscriber::fmt::layer())
        .init();
    let addr = "[::1]:50051".parse()?;
    info!(?addr, "Listening on address");
    let shell = echoshell::Shell::default();

    Server::builder()
        .add_service(echoshell::Server::with_interceptor(
            shell,
            echoshell::intercept,
        ))
        .serve(addr)
        .await?;

    Ok(())
}
