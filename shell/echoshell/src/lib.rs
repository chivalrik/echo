#![feature(map_try_insert)]
#![feature(lint_reasons)]
use std::collections::HashMap;

use generated::api::{
    echoshell_server::Echoshell, Echo, Echoes, Pearl as ApiPearl, Pearlless, Realm, Territory,
    Whisper, World,
};
use std::sync::Mutex;
use tonic::{Request, Response, Status};
use tracing::debug;
use tracing_subscriber::field::debug;

mod generated;
use crate::generated::api::{Location, PearlChallenge};
pub use generated::api::echoshell_server::EchoshellServer as Server;
// tonic::include_proto!("echo");

#[derive(Debug, Default, Clone)]
pub struct Pearl {
    public_key: String,
    challenge: String,
}

#[derive(Debug, Default)]
pub struct Shell {
    echoes: Mutex<HashMap<u32, Vec<Echo>>>,
    pearls: Mutex<HashMap<String, Pearl>>,
}

#[tonic::async_trait]
impl Echoshell for Shell {
    #[tracing::instrument(skip(self))]
    async fn utter(&self, request: Request<Whisper>) -> Result<Response<Echo>, tonic::Status> {
        let whisper = request.into_inner();
        let territory = whisper
            .territory
            // TODO Check territory against excel
            .ok_or_else(|| Status::invalid_argument("Need to know where you are whispering"))?
            .v;
        println!("I hear a whisper {whisper:?}");
        #[allow(
            clippy::cast_possible_truncation,
            reason = "Micros from epoch should not overflow u64 in expected project lifetime."
        )]
        let echo = Echo {
            id: std::time::SystemTime::now()
                .duration_since(std::time::UNIX_EPOCH)
                .map_err(|_e| tonic::Status::internal("We are broken. Please come back later"))?
                .as_micros() as u64,
            ..whisper
                .echo
                .ok_or_else(|| tonic::Status::invalid_argument("You whispered nothing."))?
        };
        debug!(?echo, "\nCreated Echo");
        let mut lock = self.echoes.lock().unwrap();
        debug!("Aquired lock");
        lock.entry(territory)
            .and_modify(|e| e.push(echo))
            .or_insert_with(|| vec![echo]);
        Ok(Response::new(dbg!(echo)))
    }

    #[tracing::instrument(skip(self))]
    async fn hear(
        &self,
        request: tonic::Request<Location>,
    ) -> Result<tonic::Response<Echoes>, tonic::Status> {
        let location = request.into_inner();
        debug!(?location);
        let territory = location.territory.unwrap().v;
        let lock = self.echoes.lock().unwrap();
        let echoes = lock.get(&territory);
        Ok(Response::new(Echoes {
            territory: Some(Territory { v: territory }),
            echo: echoes.map_or_else(Vec::new, Clone::clone),
        }))
    }

    #[tracing::instrument(skip(self))]
    async fn hear_own(
        &self,
        request: Request<World>,
    ) -> Result<tonic::Response<Echoes>, tonic::Status> {
        let message = request.into_inner();
        let location = message
            .location
            // TODO Check territory against excel
            .ok_or_else(|| Status::invalid_argument("Need to know where you are whispering"))?;
        let territory = location
            .territory
            .ok_or_else(|| Status::invalid_argument("Need to know where you are whispering"))?
            .v;
        let lock = self.echoes.lock().unwrap();
        let echoes = lock.get(&territory);
        Ok(Response::new(Echoes {
            territory: Some(Territory { v: territory }),
            echo: echoes.map_or_else(Vec::new, Clone::clone),
        }))
    }

    #[tracing::instrument(skip(self))]
    async fn hear_nearby(
        &self,
        request: Request<Realm>,
    ) -> Result<tonic::Response<Echoes>, tonic::Status> {
        let message = request.into_inner();
        println!("Got {message:?}");
        let location = message
            .location
            // TODO Check territory against excel
            .ok_or_else(|| Status::invalid_argument("Need to know where you are whispering"))?;
        let territory = location
            .territory
            .ok_or_else(|| Status::invalid_argument("Need to know where you are whispering"))?
            .v;
        let lock = self.echoes.lock().unwrap();
        let echoes = lock.get(&territory);
        Ok(Response::new(Echoes {
            territory: Some(Territory { v: territory }),
            echo: echoes.map_or_else(Vec::new, Clone::clone),
        }))
    }

    #[tracing::instrument(skip(self))]
    async fn acquire(
        &self,
        request: Request<Pearlless>,
    ) -> Result<tonic::Response<PearlChallenge>, tonic::Status> {
        let pearlless = request.into_inner();
        // TODO ContentID verification etc
        let pearl = self
            .pearls
            .lock()
            .unwrap()
            .entry(pearlless.character_name)
            .or_insert_with(|| Pearl {
                public_key: pearlless.pub_key.clone(),
                challenge: "TODOchallange".into(),
            })
            .clone(); // TODO public key is bytes, so Clone no copy?
        if pearl.public_key == pearlless.pub_key {
            debug!(?pearl, "Got this peal");
            Ok(tonic::Response::new(PearlChallenge {
                challenge: "TODOchallange".into(),
            }))
        } else {
            Err(tonic::Status::already_exists("You are not pearlless."))
        }
    }

    async fn confirm(
        &self,
        request: Request<PearlChallenge>,
    ) -> Result<Response<ApiPearl>, Status> {
        todo!()
    }
}

pub fn intercept(mut req: Request<()>) -> Result<Request<()>, Status> {
    println!("Intercepting request: {:?}", req);
    let metadata = req.metadata();
    debug!(?metadata);
    Ok(req)
}

pub fn run(who: &str) -> Result<(), String> {
    println!("Its alive from {who}");
    Ok(())
}
