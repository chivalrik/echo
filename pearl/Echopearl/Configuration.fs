﻿namespace Echopearl

open System
open Dalamud.Configuration

[<Serializable>]
type Configuration() =

    interface IPluginConfiguration with
        member val Version = 0 with get, set

//module Config =
//
