﻿module Echopearl.Whisper

open ImGuiNET

let phrases =
    let values = System.Enum.GetValues<Api.Template.PhraseTemplate>()
    values
    |> Array.map (fun t -> System.String.Format(Domain.Conversion.phraseFormat (t), "****"))

let mutable currentPhrase: int = 0
let mutable currentAdversary: int = 0
let mutable echo = ValueNone

let drawWhisperUi () =
    if ImGui.Begin("Whisper###Echopearl.CreateNewWhisper") then
        if ImGui.Combo("Choose Phrase", &currentPhrase, phrases, phrases.Length) then
            ()

        let adversaries: string [] = System.Enum.GetNames<Api.Template.X.Adversary>()

        if ImGui.Combo("Choose X", &currentAdversary, adversaries, adversaries.Length) then
            ()

        if ImGui.Button("Whisper into the Aether") then
            let echoDto =
                Shell.whisperSingle
                    Statics.ClientState.LocalPlayer.Position
                    Statics.ClientState.TerritoryType
                    (enum<Api.Template.PhraseTemplate> currentPhrase)
                    (enum<Api.Template.X.Adversary> currentAdversary)
            echo <- Domain.Conversion.parseEcho echoDto

    ImGui.End()
