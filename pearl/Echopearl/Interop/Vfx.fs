﻿module Echopearl.Interop.Vfx

open System.Numerics
open Echopearl
open Microsoft.FSharp.NativeInterop
open Helpers

#nowarn "9"
// Thanks to Tourists by Anna

[<Measure>]
type vfx
// name * pool -> vfx
type private CreateStaticVfx = delegate of string * string -> nativeint<vfx>
// vfx * unk1 * unk2 -> ?
type private PlayStaticVfx = delegate of nativeint<vfx> * float32 * int -> nativeint
// vfx -> ?
type private RemoveStaticVfx = delegate of nativeint<vfx> -> nativeint

let private createStaticVfxNative: CreateStaticVfx =
    "E8 ?? ?? ?? ?? F3 0F 10 35 ?? ?? ?? ?? 48 89 43 08"
    |> createDelegate

let private playStaticVfxNative: PlayStaticVfx =
    "E8 ?? ?? ?? ?? 8B 4B 7C 85 C9" |> createDelegate

let private removeStaticVfxNative: RemoveStaticVfx =
    //"40 53 48 83 EC 20 48 8B D9 48 8B 89 ?? ?? ?? ?? 48 85 C9 74 28 33 D2 E8 ?? ?? ?? ?? 48 8B 8B ?? ?? ?? ?? 48 85 C9" //longer from VFXEditor
    "40 53 48 83 EC 20 48 8B D9 48 8B 89 ?? ?? ?? ?? 48 85 C9 74 28 33 D2"
    |> createDelegate

let createStaticVfx name =
    createStaticVfxNative.Invoke(name, "Client.System.Scheduler.Instance.VfxObject")
//    let setPos =
//        System.IntPtr.Add(vfx, 80)
//        |> NativePtr.ofNativeInt<float32>
//        |> NativePtr.set
//
//    //    ([|0; 1 ;2|], [|position.X; position.Z; position.Y|])
////    ||> (Array.iter2 <| setPos)
//    setPos 0 position.X
//    setPos 1 position.Z
//    setPos 2 position.Y
//    NativePtr.set pos 0 position.X
//    NativePtr.set pos 1 position.Z
//    NativePtr.set pos 2 position.Y

//    let setPos =
//        System.IntPtr.Add(vfx, 640)
//        |> NativePtr.ofNativeInt<float32>
//        |> NativePtr.set
//
//    setPos 0 0f
//    setPos 1 0f
//    setPos 2 0f
//    NativePtr.set pos 0 0f
//    NativePtr.set pos 1 0f
//    NativePtr.set pos 2 0f

//    let t =
//        System.IntPtr.Add(vfx, 56)
//        |> NativePtr.ofNativeInt<byte>
//
//    NativePtr.write t ((NativePtr.read t) ||| 2uy)

//    NativePtr.write
//        (System.IntPtr.Add(vfx, 652)
//         |> NativePtr.ofNativeInt<int32>)
//        0

//    vfxMeasure
//LanguagePrimitives.IntPtrWithMeasure vfx


let playStaticVfx vfx =
    playStaticVfxNative.Invoke(vfx, 0f, -1) = System.IntPtr.Zero

let removeStaticVfx vfx =
    removeStaticVfxNative.Invoke(vfx) |> ignore

let spawnAndPlayVfx name (position: Vector3) =
    let vfxMeasured = createStaticVfx name
    playStaticVfx vfxMeasured |> ignore
    let vfx = nativeint vfxMeasured

    let pos = NativePtr.toByRef (vfx + 80 |> NativePtr.ofNativeInt<Vector3>)
    pos <- position

    let setPos =
        vfx + 640
        |> NativePtr.ofNativeInt<float32>
        |> NativePtr.set

    setPos 0 0f
    setPos 1 0f
    setPos 2 0f

    let t = vfx + 56 |> NativePtr.ofNativeInt<byte>

    ((NativePtr.read t) ||| 2uy) |> NativePtr.write t

    NativePtr.write (vfx + 652 |> NativePtr.ofNativeInt<int32>) 0

    vfxMeasured
