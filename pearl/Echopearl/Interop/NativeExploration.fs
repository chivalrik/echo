﻿namespace Echopearl.Interop

open System.Runtime.InteropServices
open Dalamud.Hooking
open Dalamud.Logging
open Echopearl
open FFXIVClientStructs.FFXIV.Client.Game
open Helpers
open Microsoft.FSharp.NativeInterop

module SetControllerStateTesting =
    [<Literal>]
    let private ControllerPollSig =
        "40 ?? 57 41 ?? 48 81 EC ?? ?? ?? ?? 44 0F ?? ?? ?? ?? ?? ?? ?? 48 8B"

    [<Measure>]
    type controllerStruct

    type private ControllerPollDelegate = delegate of nativeint<controllerStruct> -> int

    let mutable private controllerStruct: nativeint<controllerStruct> =
        0n<controllerStruct>

    let rec private maybeControllerPollHook: Hook<ControllerPollDelegate> =
        Hook<ControllerPollDelegate>.FromAddress (Statics.SigScanner.ScanText(ControllerPollSig), controllerPollDetour)

    and private controllerPollDetour maybeControllerStruct =
        PluginLog.Debug "Controller Poll in free static class!"
        controllerStruct <- maybeControllerStruct
        maybeControllerPollHook.Disable()
        maybeControllerPollHook.Original.Invoke(maybeControllerStruct)

    let start =
        PluginLog.Debug($"HookControllerPoll module start!")
        maybeControllerPollHook.Enable()

    let dispose () =
        PluginLog.Debug($"HookControllerPoll module dispose!")
        //  Dispose should be enough to disable
        // maybeControllerPollHook.Disable()
        //maybeControllerPollHook.Dispose()

        maybeControllerPollHook ?> dispose |> ignore

    [<Measure>]
    type percent
    //type Percent = int<percent>

    [<UnmanagedFunctionPointer(CallingConvention.ThisCall)>]
    type private FFXIVSetState = delegate of nativeint<controllerStruct> * rightMotorSpeed: int<percent> * leftMotorSpeed: int<percent> -> unit

    // Without this, fantomas' silly auto format of the above fails to compile OTZ.
    // TODO Research how to disbale the autoformat (max Line?) above
    type private Dummy =
        interface
        end

    let private ffxivSetStateNative: FFXIVSetState =
        "40 55 53 56 48 8b ec 48 81 ec 80 00 00 00 33 f6 44 8b d2 4c 8b c9"
        |> Helpers.createDelegate

    let ffxivSetState (rM: int<percent>) (lM: int<percent>) =
        ffxivSetStateNative.Invoke(controllerStruct, rM, lM)

    let exp () =
        let managedByref = NativePtr.toByRef (ActionManager.Instance())
        let castDetail = NativePtr.toByRef (managedByref.GetRecastGroupDetail(57))
        ()
