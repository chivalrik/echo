﻿namespace Echopearl

open Api
open Grpc.Net.Client



module PearlExploration =
    let whisper () =
        use channel = GrpcChannel.ForAddress("http://[::1]:50051")
        let client = Echoshell.EchoshellClient(channel)

        let sentence =
            Domain.Conversion.templateSingle Template.PhraseTemplate.Ahhcommaxdotdotdot Template.X.Thing.Coffin

        //        let sentence =
//            { Template.Single.empty () with
//                Phrase = ValueSome Template.Phrase.Ahhcommaxdotdotdot
//                X = ValueSome(Template.Single.Types.X.Thing Template.X.Thing.Coffin) }

        let request =
            { Whisper.empty () with
                Echo =
                    ValueSome
                        { Echo.empty () with
                            Spoiler = ValueSome false
                            X = ValueSome 1f
                            Y = ValueSome 2f
                            Z = ValueSome 3f
                            Type = ValueSome(Echo.Types.Single sentence)
                        }
                Territory =
                    ValueSome
                        { Territory.empty () with
                            V = ValueSome 56u
                        }
                DataCenter = ValueSome 4u
                World = ValueSome 2u
            }

        let r = client.UtterAsync request
        let r = Echoshell.EchoshellClient.Functions.utterAsync (client) request
        let r = r.ResponseAsync.Result
        printf $"Spoiler? %b{r.Spoiler.IsSome}"

//https://sharplab.io/

//#nowarn "42"
// You can actually use this but you have to specify the --compiling-fslib (undocumented) and --standalone flags in your code.
//module NativePtrExtension =
//    [<NoDynamicInvocation>]
//    let inline readInt8 (address: nativeint) (offset: int32) : int8 = (# "add ldind.i1" address offset: int8 #)
//
//    [<NoDynamicInvocation>]
//    let inline writeInt8 (address: nativeint) (offset: int32) (value: int8) : unit = (# "stind.i1" (# "add" address offset : nativeint #) value #)
