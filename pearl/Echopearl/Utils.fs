﻿namespace rec Echopearl

open System
open System.Runtime.InteropServices
open Dalamud.Data
open Dalamud.Game
open Dalamud.Game.ClientState
open Dalamud.Game.ClientState.Aetherytes
open Dalamud.Game.ClientState.Buddy
open Dalamud.Game.ClientState.Conditions
open Dalamud.Game.ClientState.Fates
open Dalamud.Game.ClientState.GamePad
open Dalamud.Game.ClientState.Keys
open Dalamud.Game.ClientState.Objects
open Dalamud.Game.Command
open Dalamud.Game.Gui
open Dalamud.Game.Gui.ContextMenus
open Dalamud.Game.Gui.Dtr
open Dalamud.Game.Gui.FlyText
open Dalamud.Game.Gui.PartyFinder
open Dalamud.Game.Gui.Toast
open Dalamud.Game.Libc
open Dalamud.Game.Network
open Dalamud.IoC
open Dalamud.Plugin

type Statics() =
    [<PluginService; RequiredVersion("1.0")>]
    static member val Interface: DalamudPluginInterface = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val ClientState: ClientState = null with get, set

    [<PluginService>]
    static member val SigScanner: SigScanner = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val CommandManager: CommandManager = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val GameGui: GameGui = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val Framework: Framework = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val DataManager: DataManager = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val ChatHandler: ChatHandlers = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val GameNetwork: GameNetwork = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val LibcFunction: LibcFunction = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val ChatGui: ChatGui = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val ToastGui: ToastGui = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val PartyFinderGui: PartyFinderGui = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val FlyTextGui: FlyTextGui = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val DtrBar: DtrBar = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val ContextMenu: ContextMenu = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val AetheryteList: AetheryteList = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val BuddyList: BuddyList = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val Condition: Condition = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val FateTable: FateTable = null with get, set

    [<PluginService; RequiredVersion("1.0.0")>]
    static member val GamepadState: GamepadState = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val KeyState: KeyState = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val ObjectTable: ObjectTable = null with get, set

    [<PluginService; RequiredVersion("1.0")>]
    static member val TargetManager: TargetManager = null with get, set

module Helpers =
    let inline createDelegate signature : ^a =
        signature
        |> Statics.SigScanner.ScanText
        |> Marshal.GetDelegateForFunctionPointer

    let dispose (h: IDisposable) = h.Dispose()


    [<Struct>]
    type WasNull = | V

    let inline (?>) (i: ^a when ^a: null) (f: ^a -> ^b) =
        match i with
        | null -> ValueNone
        | _ -> ValueSome(f i)

    let inline (+) (n: nativeint) a = IntPtr.Add(n, a)

module Constant =
    [<Literal>]
    let PluginName = "Echopearl"
