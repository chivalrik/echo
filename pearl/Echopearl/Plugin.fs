namespace rec Echopearl

open System
open Dalamud.Game
open Dalamud.Logging
open Dalamud.Plugin

type Plugin(pi: DalamudPluginInterface) =
    [<Literal>]
    let Command = "/echopearl"
    let mutable disposed = false
    let hearFramework = Framework.OnUpdateDelegate Hear.frameworkUpdate

    do
        if not (Statics() |> pi.Inject) then
            PluginLog.Fatal "Could not inject statics, throwing to abort."
            disposed <- true
            failwith "Could not inject statics, that's a big error and as such, I quit."

        PluginLog.Information "Statics injected."
        //        Shell.obtainPearl ()

        // TODO Nope, does not work, not same reference, need to save
        Action Debug.draw
        |> Statics.Interface.UiBuilder.add_Draw

        Action Whisper.drawWhisperUi
        |> Statics.Interface.UiBuilder.add_Draw

        Action Listen.drawListenUI
        |> Statics.Interface.UiBuilder.add_Draw

        Framework.OnUpdateDelegate Debug.frameworkUpdate
        |> Statics.Framework.add_Update

        EventHandler<uint16> Hear.territoryChanged
        |> Statics.ClientState.TerritoryChanged.AddHandler

        hearFramework |> Statics.Framework.add_Update

    member self.Dispose(disposing: bool) =
        if not disposed then
            disposed <- true

            if disposing then
                // Managed ressource cleanup here
                Action Debug.draw
                |> Statics.Interface.UiBuilder.remove_Draw

                Action Whisper.drawWhisperUi
                |> Statics.Interface.UiBuilder.remove_Draw

                Action Listen.drawListenUI
                |> Statics.Interface.UiBuilder.remove_Draw

                Framework.OnUpdateDelegate Debug.frameworkUpdate
                |> Statics.Framework.remove_Update

                EventHandler<uint16> Hear.territoryChanged
                |> Statics.ClientState.TerritoryChanged.RemoveHandler

                hearFramework |> Statics.Framework.remove_Update

                Shell.dispose ()

            //Unmanaged resource cleanup here
            Echopearl.Interop.SetControllerStateTesting.dispose ()
            Debug.dispose ()
            ()



    override self.Finalize() = self.Dispose(false)

    interface IDalamudPlugin with
        member self.Dispose() =
            self.Dispose(true)
            GC.SuppressFinalize(self)

        member Self.Name = Constant.PluginName



//    pi.Create<PearlPlugin>() |> ignore
//    static let mutable _pi: DalamudPluginInterface = null
//    [<DefaultValue>]
//    [<PluginService>]
//    static val mutable private P: DalamudPluginInterface
//    [<PluginService>]
//    [<PluginService>]
//    static member PluginInterface
//        with public get() = _pi
//        and private set value = _pi <- value
