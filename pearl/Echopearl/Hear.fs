﻿module Echopearl.Hear

open System
open Dalamud.Game
open Grpc.Core
open Interop.Vfx
open Helpers

let mutable private echoes: Domain.Echo seq = Seq.empty
let mutable private echoVfxs: nativeint<vfx> seq = Seq.empty
let mutable response: AsyncUnaryCall<Api.Echoes> voption = ValueNone

let mutable next: DateTime = DateTime.Now
let private waitSpan = TimeSpan.FromMinutes(5)

let private vfxName = "bgcommon/world/common/vfx_for_live/eff/b0810_tnsk_y.avfx"
//let vfxName = "bgcommon/world/common/vfx_for_live/eff/b0134_rabs_y.avfx"
//let vfxName = "bgcommon/world/common/vfx_for_live/eff/b0049_ensp_y.avfx"

let frameworkUpdate (_framework: Framework) =
    //    PluginLog.Information "Hear FrameworUpdate"
    if response.IsSome then
        if response.Value.ResponseAsync.IsCompletedSuccessfully then
            echoes <-
                response.Value.ResponseAsync.Result.Echo
                |> Seq.map Domain.Conversion.parseEcho
                |> Seq.filter ValueOption.isSome
                |> Seq.map ValueOption.get
            response <- ValueNone
            echoVfxs |> Seq.iter removeStaticVfx
            echoVfxs <-
                echoes
                |> Seq.map (fun e -> spawnAndPlayVfx vfxName e.Position)

            ()
    else if
        (DateTime.Now - next) > waitSpan
        && not (isNull Statics.ClientState.LocalPlayer)
    then
        response |> ValueOption.iter dispose
        // Technically, LocalPlayer could have changed already and be null, but unlikely in the same FrameworkUpdate, no?
        response <-
            Shell.hear Statics.ClientState.TerritoryType Statics.ClientState.LocalPlayer.Position
            |> ValueSome
        next <- DateTime.Now.Add waitSpan
    ()

let territoryChanged _sender _territory = next <- DateTime.Now
