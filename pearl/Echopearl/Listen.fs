﻿module Echopearl.Listen

open ImGuiNET
open Microsoft.FSharp.Core

let mutable listen = false

let drawListenUI () =
    if (Whisper.echo.IsNone || not listen) then
        listen <- false
        ()
    else
        let echo = Whisper.echo.Value

        if ImGui.Begin("Listen###echopearl.reading_echo", &listen) then
            ImGui.Text echo.SentenceFirst
            echo.SentenceSecond |> ValueOption.iter ImGui.Text

        ImGui.End()
