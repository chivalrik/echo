﻿module Echopearl.Shell

open System.Numerics
open Dalamud.Logging
open Grpc.Net.Client

let private channel = GrpcChannel.ForAddress("http://[::1]:50051")
let private client = Api.Echoshell.EchoshellClient(channel)

let mutable private challenge = ValueNone

let obtainPearl () =
    let request =
        { Api.Pearlless.empty () with
            World = ValueSome "TODO"
            CharacterName = ValueSome "TODO"
            PubKey = ValueSome "TODO"
        }

    let pearl = client.Acquire(request)
    challenge <- pearl.Challenge
    PluginLog.Debug $"Got {challenge}"

let hear (territory: uint16) (position: Vector3) =
    let request =
        { Api.Location.empty () with
            Territory =
                { Api.Territory.empty () with
                    V = (uint32 territory) |> ValueSome
                }
                |> ValueSome
            X = ValueSome position.X
            Y = ValueSome position.Y
            Z = ValueSome position.Z
        }
    client.HearAsync request

let private utter (position: Vector3) (territory: uint16) (sentence: Api.Echo.Types.Type) =
    let request =
        { Api.Whisper.empty () with
            Territory =
                { Api.Territory.empty () with
                    V = (uint32 territory) |> ValueSome
                }
                |> ValueSome
            // TODO Workd + Datacenter....do we pass that all in or cheat and ask Statics.ClientState?
            Echo =
                ValueSome
                    { Api.Echo.empty () with
                        Spoiler = ValueSome true
                        X = ValueSome position.X
                        Y = ValueSome position.Y
                        Z = ValueSome position.Z
                        Type = ValueSome sentence
                    }
        }

    client.Utter(request, deadline = System.DateTime.UtcNow.AddSeconds(10))

let whisperSingle (position: Vector3) (territory: uint16) phrase x =
    let sentence = Domain.Conversion.templateSingle phrase x
    utter position territory (Api.Echo.Types.Single sentence)

let whisperDouble (position: Vector3) (territory: uint16) phrase x phrase2 x2 conjunction =
    let sentence = Domain.Conversion.templateDouble phrase x phrase2 x2 conjunction
    utter position territory (Api.Echo.Types.Double sentence)

let dispose () = channel.Dispose()
