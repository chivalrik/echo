﻿module Echopearl.Debug

open System
open System.Numerics
open Dalamud.Logging
open FFXIVClientStructs.FFXIV.Client.UI.Agent
open ImGuiNET
open Echopearl.Interop.SetControllerStateTesting
open Microsoft.FSharp.NativeInterop
open Interop.Vfx

let mutable visible = true
let mutable vfx = 0n<vfx>
let mutable queued = false

let dispose () =
    if (nativeint vfx) <> IntPtr.Zero then
        removeStaticVfx vfx |> ignore

let contentId _ =
    let id = Statics.ClientState.LocalContentId
    //ImGui.Text $"ContentID PEARL%016x{id}"
    ()

let draw () =
    if not visible then
        ()
    else
        if ImGui.Begin($"%s{Constant.PluginName} -- Debug", &visible) then
            let player = Statics.ClientState.LocalPlayer
            let agentMap = (NativePtr.toByRef (AgentMap.Instance()))

            ImGui.Text
                $"Phrase: {Enum.GetValues<Api.Template.PhraseTemplate>()[Whisper.currentPhrase]}; Adversary {Enum.GetValues<Api.Template.X.Adversary>()[Whisper.currentAdversary]}"

            ImGui.Text($"AgentMap Territory: {agentMap.CurrentTerritoryId}")
            ImGui.Text($"AgentMap Selected Territory: {agentMap.SelectedTerritoryId}")
            ImGui.Text($"ClientState TerritoryType: {Statics.ClientState.TerritoryType}")
            if not (isNull player) then
                ImGui.Text($"ClientState CurrentWorld: {Statics.ClientState.LocalPlayer.CurrentWorld.Id}")
                ImGui.Text($"ClientState CurrentWorld: {Statics.ClientState.LocalPlayer.CurrentWorld.GameData.Name.RawString}")
                ImGui.Text($"ClientState CurrentWorld DC: {Statics.ClientState.LocalPlayer.CurrentWorld.GameData.DataCenter.Value.Name.RawString}")

            contentId ()

            if ImGui.Button("Disable (will be re-enabled in 5 sec)") then
                PluginLog.Debug $"Visible is %b{visible}"
                visible <- false

                task {
                    let! _ = Async.Sleep(TimeSpan.FromSeconds(5))
                    visible <- true
                    ()
                }
                |> ignore

                ()
            //            ImGui.Text $"Value of %s{nameof HookControllerPoll.controllerStruct}: %x{HookControllerPoll.controllerStruct}"
            if ImGui.Button("Vibrate!") then
                ffxivSetState 50<percent> 50<percent>

            if ImGui.Button("STOP!") then
                ffxivSetState 0<percent> 0<percent>

            if (nativeint vfx) <> IntPtr.Zero then
                if ImGui.Button("Remove VFX") then
                    removeStaticVfx vfx |> ignore
                    vfx <- 0n<vfx>
                    Whisper.echo <- ValueNone

            if (nativeint vfx) <> IntPtr.Zero then
                let vfxPos =
                    IntPtr.Add((nativeint vfx), 80)
                    |> NativePtr.ofNativeInt<Vector3>
                    |> NativePtr.read

                ImGui.Text($"Vfx position %s{vfxPos.ToString()}")

            if not (isNull player) then
                let mutable pos = player.Position
                ImGui.Text($"Player position %s{pos.ToString()}")

            if (Whisper.echo.IsSome)
               && ((nativeint vfx) <> IntPtr.Zero) then
                if ImGui.Button("Listen to Echo") then
                    Listen.listen <- true

            if (Whisper.echo.IsSome)
               && ((nativeint vfx) = IntPtr.Zero) then
                let vfxName = "bgcommon/world/common/vfx_for_live/eff/b0810_tnsk_y.avfx"
                //                let vfxName = "bgcommon/world/common/vfx_for_live/eff/b0134_rabs_y.avfx"
//                let vfxName = "bgcommon/world/common/vfx_for_live/eff/b0049_ensp_y.avfx"
                vfx <- spawnAndPlayVfx vfxName Whisper.echo.Value.Position

        //                    Statics.Framework.RunOnFrameworkThread(fun () -> playStaticVfx (vfx))
//                    |> ignore
        //queued <- true

        ImGui.End()

let frameworkUpdate _framework =
    if queued then
        playStaticVfx (vfx) |> ignore
        queued <- false
