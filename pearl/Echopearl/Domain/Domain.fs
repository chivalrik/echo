﻿namespace Echopearl.Domain

open System.Numerics

type Echo =
    { Id: uint64
      Position: Vector3
      Spoiler: bool
      UpVotes: uint32
      DownVotes: uint32
      SentenceFirst: string
      SentenceSecond: string voption }
