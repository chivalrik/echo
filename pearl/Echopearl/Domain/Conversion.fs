﻿module Echopearl.Domain.Conversion

open System

open System.Numerics
open Api
open Microsoft.FSharp.Core

let FailedMatch t =
    $"Unknown for {t}, update your Echopearl. Contact your local Echoshell support if that does not help."

//(x : ^a when ^a :> System.Enum and ^a :> System.ValueType and
let templateSingle template (x: obj) =
    let wrapped =
        match x with
        | :? Template.X.Action as x -> Template.Single.Types.Action x
        | :? Template.X.Adversary as x -> Template.Single.Types.Adversary x
        | :? Template.X.Affinity as x -> Template.Single.Types.Affinity x
        | :? Template.X.BattleTactic as x -> Template.Single.Types.BattleTactic x
        | :? Template.X.BodyPart as x -> Template.Single.Types.BodyPart x
        | :? Template.X.Concept as x -> Template.Single.Types.Concept x
        | :? Template.X.Direction as x -> Template.Single.Types.Direction x
        | :? Template.X.People as x -> Template.Single.Types.People x
        | :? Template.X.Phrase as x -> Template.Single.Types.Phrase x
        | :? Template.X.Place as x -> Template.Single.Types.Place x
        | :? Template.X.Situation as x -> Template.Single.Types.Situation x
        | :? Template.X.Thing as x -> Template.Single.Types.Thing x
        | _ -> Template.Single.Types.Thing Template.X.Thing.Bug // TODO voption return

    { Template.Single.empty () with
        Template = ValueSome template
        X = ValueSome wrapped
    }

let templateDouble template1 (x1: obj) template2 (x2: obj) conjunction =
    let first = templateSingle template1 x1
    let second = templateSingle template2 x2
    { Template.Double.empty () with
        First = ValueSome first
        Second = ValueSome second
        Conjunction = ValueSome conjunction
    }

module x =
    let adversary a =
        match a with
        | Api.Template.X.Adversary.Enemy -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Weakfoe -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Strongfoe -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Monster -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Dragon -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Boss -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Sentry -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Group -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Pack -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Decoy -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Undead -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Soldier -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.KnightAdversary -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Cavalier -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Archer -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Sniper -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Mage -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Ordnance -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.MonarchAdversary -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.LordAdversary -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Demihuman -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Outsider -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Giant -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Horse -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Dog -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Wolf -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Rat -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Beast -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Bird -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Raptor -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Snake -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Crab -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Prawn -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Octopus -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.BugEnemy -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Scarab -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Slug -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Wraith -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Skeleton -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Monstrosity -> Enum.GetName<Api.Template.X.Adversary>(a)
        | Api.Template.X.Adversary.Illomenedcreature -> Enum.GetName<Api.Template.X.Adversary>(a)
        | _ -> FailedMatch "Adversary"

    let extractX x =
        // TODO What does proto do when deserializing enum value not in our client's enum?  Unknownfields?
        match x with
        | Api.Template.Single.Types.Action item -> item.ToString()
        | Api.Template.Single.Types.Adversary item -> item.ToString()
        | Api.Template.Single.Types.Affinity item -> item.ToString()
        | Api.Template.Single.Types.BattleTactic item -> item.ToString()
        | Api.Template.Single.Types.BodyPart item -> item.ToString()
        | Api.Template.Single.Types.Concept item -> item.ToString()
        | Api.Template.Single.Types.Direction item -> item.ToString()
        | Api.Template.Single.Types.People item -> item.ToString()
        | Api.Template.Single.Types.Phrase item -> item.ToString()
        | Api.Template.Single.Types.Place item -> item.ToString()
        | Api.Template.Single.Types.Situation item -> item.ToString()
        | Api.Template.Single.Types.Thing item -> item.ToString()

let phraseFormat phrase =
    match phrase with
    | Api.Template.PhraseTemplate.Xahead -> "{0} ahead"
    | Api.Template.PhraseTemplate.Likelyx -> "Likely {0}"
    | Api.Template.PhraseTemplate.Ifonlyihadaxdotdotdot -> "If only I had a {0}..."
    | Api.Template.PhraseTemplate.Xcomma -> "{0},"
    | Api.Template.PhraseTemplate.Ox -> "O {0}"
    | Api.Template.PhraseTemplate.Ahhcommaxdotdotdot -> "Ahh, {0}..."
    | Api.Template.PhraseTemplate.Noxahead -> "No {0} ahead"
    | Api.Template.PhraseTemplate.Firstoffcommax -> "First off, {0}"
    | Api.Template.PhraseTemplate.Didnotexpectxdotdotdot -> "Didn't expect {0}..."
    | Api.Template.PhraseTemplate.Beholdcommaxexclamation -> "Behold, {0}!"
    | Api.Template.PhraseTemplate.X -> "{0}"
    | Api.Template.PhraseTemplate.Xrequiredahead -> "{0} required ahead"
    | Api.Template.PhraseTemplate.Seekx -> "Seek {0}"
    | Api.Template.PhraseTemplate.Visionsofxdotdotdot -> "Visions of {0}..."
    | Api.Template.PhraseTemplate.Offerx -> "Offer {0}"
    | Api.Template.PhraseTemplate.Xexclamation -> "{0}!"
    | Api.Template.PhraseTemplate.Bewaryofx -> "Be wary of {0}"
    | Api.Template.PhraseTemplate.Stillnoxdotdotdot -> "Still no {0}..."
    | Api.Template.PhraseTemplate.Couldthisbeaxquestion -> "Could this be a {0}?"
    | Api.Template.PhraseTemplate.Praisethex -> "Praise the {0}"
    | Api.Template.PhraseTemplate.Xquestion -> "{0}?"
    | Api.Template.PhraseTemplate.Tryx -> "Try {0}"
    | Api.Template.PhraseTemplate.Whyisitalwaysxquestion -> "Why is it always {0}?"
    | Api.Template.PhraseTemplate.Timeforx -> "Time for {0}"
    | Api.Template.PhraseTemplate.Lettherebex -> "Let there be {0}"
    | Api.Template.PhraseTemplate.Xdotdotdot -> "{0}..."
    | _ -> FailedMatch "Phrasetemplate"

let extractConjunction c =
    match c with
    | Template.Conjunction.Andthen -> "and then {0}"
    | Template.Conjunction.Or -> "or {0}"
    | Template.Conjunction.But -> "but {0}"
    | Template.Conjunction.Therefore -> "therefore {0}"
    | Template.Conjunction.Inshort -> "in short {0}"
    | Template.Conjunction.Except -> "except {0}"
    | Template.Conjunction.Bytheway -> "by the way {0}"
    | Template.Conjunction.Sotospeak -> "so to speak {0}"
    | Template.Conjunction.Allthemore -> "all the more {0}"
    | Template.Conjunction.Comma -> "{0},"
    | Template.Conjunction.Fullstop -> "{0}."
    | _ -> FailedMatch "Conjunction"

let extractSingleSentence (single: Api.Template.Single) =
    let template =
        single.Template
        |> ValueOption.defaultValue Api.Template.PhraseTemplate.Xahead
        |> phraseFormat
    let x =
        single.X
        |> ValueOption.defaultValue (Api.Template.Single.Types.Thing Api.Template.X.Thing.Bug)
        |> x.extractX
    String.Format(template, x)

open Echo.Types

let extractEchoSentences (t: Type voption) =
    match t with
    | ValueNone -> struct (String.Empty, ValueNone)
    | ValueSome t ->
        match t with
        | Type.Single s -> struct (extractSingleSentence s, ValueNone)
        | Type.Double d ->
            (d.First, d.Second, d.Conjunction)
            |||> ValueOption.map3 (fun f s c ->
                let fs = extractSingleSentence f
                let ss = extractSingleSentence s
                let ct = extractConjunction c
                match c with
                | Template.Conjunction.Fullstop
                | Template.Conjunction.Comma -> struct (String.Format(ct, fs), ss |> ValueSome)
                | _ -> struct (fs, String.Format(ct, ss) |> ValueSome))
            |> ValueOption.defaultValue (struct (String.Empty, ValueNone))

let parseEcho (e: Api.Echo) =
    if e.Id.IsNone then
        ValueNone
    else
        let struct (fs, ss) = extractEchoSentences e.Type
        {
            Echopearl.Domain.Echo.Id = e.Id.Value
            Position = Vector3(e.X |> ValueOption.defaultValue 0f, e.Y |> ValueOption.defaultValue 0f, e.Z |> ValueOption.defaultValue 0f)
            Spoiler = e.Spoiler |> ValueOption.defaultValue false
            UpVotes = e.UpVotes |> ValueOption.defaultValue 0u
            DownVotes = e.DownVotes |> ValueOption.defaultValue 0u
            SentenceFirst = fs
            SentenceSecond = ss
        }
        |> ValueSome
