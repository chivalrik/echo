# Echopearl & Echoshell

Souls-Like messaging system in FFXIV.  
A Dalamud plugin.  
WIP

## Incomplete list of TODOs & ideas

- Become alpha ready.
- Disallow/Filter Messages placed while flying?
- Emote support (may be too complex?).
- Appraisal notification support (needed?).